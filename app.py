from flask import Flask, render_template, g, request, json
from db import DB
from product import Product
app = Flask(__name__)

@app.route("/")
def main():
	products = g.db._load_products()
	return render_template('index.html', products=products)

@app.route("/showSignUp")
def showSignUp():
	return render_template('signup.html')

@app.route("/signUp", methods=['POST', 'GET'])
def signUp():
	#read posted values from the UI
	if request.method=='POST':
		_name = request.form['username']
		_password = request.form['password']
		db.insertUser(username,password)
		users = db.retrieveUsers()
		return render_template('signup.html', users=users)
	else:
		return render_template('signup.html')


@app.route("/contact")
def contact():
	return render_template('contact.html')
	#see if they are okay
	#if _name and _email and _password:
	#   return json.dumps({'html': '<span> All good. </span>'})
	#else:
	#   return json.dumps({'html':'<span>Enter the required fields please</span>'})

@app.before_first_request
def _before_first_request():
	
	db = DB("database_name")
	try:
		db.create_tables()
		db._save_product(Product("Wrench", 10, 1))
		db._save_product(Product("Hammer", 5, 2))
		db._save_product(Product("Nail", 1, 3))
		db._save_product(Product("Nail", 1, 3))
		db._save_product(Product("Nail", 1, 3))
		db._save_product(Product("Nail", 1, 3))

	except Exception as e:
		print(str(e))
		print("Table has already been created")
	


@app.before_request
def _before_request():
	g.db = DB("database_name")


if __name__ == "__main__":
	app.run()