import sqlite3
from product import Product


class DB:
    def __init__(self, database_name):
        self.conn = sqlite3.connect(database_name)

    def create_tables(self):
        c = self.conn.cursor()
        
        c.execute(''' 
            CREATE TABLE product(
            PRODUCTID INTEGER NOT NULL PRIMARY KEY, 
            NAME VARCHAR(100), 
            PRICE INTEGER NOT NULL)''')


        self.conn.commit()

    def create_user(self):
        c = self.conn.cursor()
        
        c.execute(''' CREATE TABLE user(id INTEGER NOT NULL
        PRIMARY KEY autoincrement, username TEXT NOT NULL, password TEXT NOT NULL)''')


        self.conn.commit()

    def save_product(self, product):
        if product.product_id is None:
            self._save_product(product)
            #new product
        else:
            self._update_product(product)
            #it exists

    def _save_product(self, product):

        query = "INSERT INTO product (Name, Price) VALUES (?, ?)"

        c = self.conn.cursor()

        c.execute(query, (product.name, product.price))

        for row in c.execute('SELECT last_insert_rowid()'):
            product.product_id = row[0]
            

        self.conn.commit()

    def retrieve_product(self, product_id):
        query = "SELECT Name, Price FROM product WHERE ProductID = ?"
        c = self.conn.cursor()
        #print(product_id)
        for row in c.execute(query, tuple([product_id])):
            return Product(row[0], row[1], product_id)


    def _update_product(self, product):

        query = "UPDATE product SET name=?, price=? WHERE ProductID=?"

        c = self.conn.cursor()

        c.execute(query, (product.name, product.price, product.product_id))


    def _delete_product(self, product):

        query = "DELETE product WHERE ProductID=?"

        c = self.conn.cursor()

        c.execute(query, (product.name, product.price, product.product_id))
    
    def _load_products(self):
        query = "SELECT Name, Price FROM product"
        c = self.conn.cursor()
        plist = []
        for row in c.execute(query):
            plist.append(Product(row[0], row[1]))
        return plist


    def insertUser(self,username,password):
        #self.conn = sqlite3.connect(database_name)
        c = self.conn.cursor()
        c.execute("INSERT INTO users (username,password) VALUES (?,?)", (username,password))
        conn.commit()
        #conn.close()

    def retrieveUsers(self):
        #self.conn = sqlite3.connect(database_name)
        c = self.conn.cursor()
        c.execute("SELECT username, password FROM users")
        users = c.fetchall()
        #conn.close()
        return users